package variables_operators;

public class variables {

	public static void main(String[] args) {

		// arithmetic operators

		int a = 20, b = 10, c = 0, e = 40, f = 30;
		String x = "Thank", y = "You";

		// + and - operator
		System.out.println("a + b = " + (a + b));
		System.out.println("a - b = " + (a - b));

		// + operator if used with strings
		// concatenates the given strings.
		System.out.println("x + y = " + x + y);

		// * and / operator
		System.out.println("a * b = " + (a * b));
		System.out.println("a / b = " + (a / b));

		// modulo operator gives remainder
		// on dividing first operand with second
		System.out.println("a % b = " + (a % b));

		// arithmetic operators end

		// unary operators
		int aa = 20, bb = 10, cc = 0, dd = 20, ee = 40;
		boolean condition = true;

		// pre-increment operator
		// a = a+1 and then c = a;
		cc = ++aa;
		System.out.println("Value of cc (++aa) = " + cc);

		// post increment operator
		// c=b then b=b+1
		cc = bb++;
		System.out.println("Value of cc (bb++) = " + cc);

		// pre-decrement operator
		// d=d-1 then c=d
		cc = --dd;
		System.out.println("Value of cc (--dd) = " + cc);

		// post-decrement operator
		// c=e then e=e-1
		cc = ee--;
		System.out.println("Value of cc (ee--) = " + cc);

		// Logical not operator
		System.out.println("Value of !condition =" + !condition);
		// unary operators end

		// assignment operators

		// simple assignment operator
		c = b;
		System.out.println("Value of c = " + c);

		a = a + 1;
		b = b - 1;
		e = e * 2;
		f = f / 2;
		System.out.println("a, b, e, f = " + a + ", " + b + ", " + e + ", " + f);
		a = a - 1;
		b = b + 1;
		e = e / 2;
		f = f * 2;

		// shorthand assignment operator
		a += 1;
		b -= 1;
		e *= 2;
		f /= 2;
		System.out.println("a, b, e, f (" + "using shorthand operators)= " + a + ", " + b + ", " + e + ", " + f);

		// assignment operators end

		// ternary operator.
		int aaaaa = 20, bbbbb = 10, cccc = 30;
		int result = ((aaaaa > bbbbb) ? (aaaaa > cccc) ? aaaaa : cccc : (bbbbb > cccc) ? bbbbb : cccc);
		System.out.println("Max of three numbers = " + result);
		// ternary operator. end

		// relational operators
		int aaa = 20, bbb = 10;
		int ar[] = { 1, 2, 3 };
		int br[] = { 1, 2, 3 };
		boolean condition1 = true;

		// various conditional operators
		System.out.println("aaa == bbb :" + (aaa == bbb));
		System.out.println("aaa < bbb :" + (aaa < bbb));
		System.out.println("aaa <= bbb :" + (aaa <= bbb));
		System.out.println("aaa > bbb :" + (aaa > bbb));
		System.out.println("aaa >= bbb :" + (aaa >= bbb));
		System.out.println("aaa != bbb :" + (aaa != bbb));

		System.out.println("xxx == yyy : " + (ar == br));

		System.out.println("condition==true :" + (condition1 == true));

		// relational operators end
	}

}

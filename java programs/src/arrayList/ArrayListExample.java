package arrayList;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		int n = 5; 

        ArrayList<Integer> arrli 
            = new ArrayList<Integer>(n); 

        for (int i = 1; i <= n; i++) 
            arrli.add(i); 
   
        System.out.println(arrli); 
  
        arrli.remove(3); 
  

        System.out.println(arrli); 
  
        for (int i = 0; i < arrli.size(); i++) 
            System.out.print(arrli.get(i) + " ");
        
        
        ArrayList<String> al = new ArrayList<>(); 
        
        al.add("Suyog"); 
        al.add("Ashok"); 
        al.add(2, "Thakre"); 
  
        System.out.println(al); 
        
        
        ArrayList<String> arrlst = new ArrayList<>(); 
        
        arrlst.add("Info"); 
        arrlst.add("Pvt Ltd"); 
        arrlst.add(1, "Stretch"); 
  
        System.out.println( 
            "Initial ArrayList " + arrlst); 
  
        arrlst.remove(1); 
  
        System.out.println( 
            "After the Index Removal " + arrlst); 
  
        arrlst.remove("Pvt Ltd"); 
  
        System.out.println( 
            "After the Object Removal " + arrlst);
	}

}

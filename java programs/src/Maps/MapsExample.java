package Maps;

import java.util.*;

public class MapsExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map map = new HashMap();
		// Adding elements to map
		map.put(1, "Amit");
		map.put(5, "Rahul");
		map.put(2, "Jai");
		map.put(6, "Amit");
		// Traversing Map
		Set set = map.entrySet();
		Iterator itr = set.iterator();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		
		System.out.println();
		Map<Integer,String> maps=new HashMap<Integer,String>();  
		  maps.put(100,"Amit");  
		  maps.put(101,"Vijay");  
		  maps.put(102,"Rahul");  
		  //Elements can traverse in any order  
		  for(Map.Entry m:maps.entrySet()){  
		   System.out.println(m.getKey()+" "+m.getValue());  
		  } 
		  maps.entrySet().stream().sorted(Map.Entry.comparingByValue())  
	      .forEach(System.out::println); 
	}

}

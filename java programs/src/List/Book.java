package List;
import java.util.*;  
public class Book {
	int id;
	String name, author, publisher;
	int quantity;

	public Book(int id, String name, String author, String publisher, int quantity) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.publisher = publisher;
		this.quantity = quantity;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Book> list = new ArrayList<Book>();
		// Creating Books
		Book b1 = new Book(101, "Let us C", "Yashwant Kanetkar", "BPB", 8);
		Book b2 = new Book(102, "Data Communications and Networking", "Forouzan", "Mc Graw Hill", 4);
		Book b3 = new Book(103, "Operating System", "Galvin", "Wiley", 6);
		// Adding Books to list
		list.add(b1);
		list.add(b2);
		list.add(b3);
		// Traversing list
		for (Book b : list) {
			System.out.println(b.id + " " + b.name + " " + b.author + " " + b.publisher + " " + b.quantity);
		}

		
		  List<String> list1=new ArrayList<String>();  
		  list1.add("Mango");  
		  list1.add("Apple");  
		  list1.add("Banana");  
		  list1.add("Grapes");  
		  //Sorting the list  
		  Collections.sort(list1);  
		   //Traversing list through the for-each loop  
		  for(String fruit:list1)  
		    System.out.println(fruit);  
		      
		 System.out.println("Sorting numbers...");  
		  //Creating a list of numbers  
		  List<Integer> list2=new ArrayList<Integer>();  
		  list2.add(21);  
		  list2.add(11);  
		  list2.add(51);  
		  list2.add(1);  
		  //Sorting the list  
		  Collections.sort(list2);  
		   //Traversing list through the for-each loop  
		  for(Integer number:list2)  
		    System.out.println(number);  
		 }  
	}


package naturalSorting;

import java.util.*;

public class Student implements Comparable<Student> {
	public String name;

	public Student(String name) {
		this.name = name;
	}

	public int compareTo(Student person) {
		return name.compareTo(person.name);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Student> al = new ArrayList<Student>();
		al.add(new Student("Viru"));
		al.add(new Student("Saurav"));
		al.add(new Student("Mukesh"));
		al.add(new Student("Tahir"));

		Collections.sort(al);
		for (Student s : al) {
			System.out.println(s.name);
		}
		System.out.println();
		ArrayList<String> alr = new ArrayList<String>();
		alr.add("Viru");
		alr.add("Saurav");
		alr.add("Mukesh");
		alr.add("Tahir");

		Collections.sort(al, Collections.reverseOrder());
		Iterator i = alr.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}
}

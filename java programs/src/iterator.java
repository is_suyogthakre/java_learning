import java.util.Collection;  
import java.util.Iterator;  
import java.util.concurrent.LinkedBlockingDeque;  
public class iterator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Collection<Character> collection = new LinkedBlockingDeque<Character>();  
        for (char c = 'a'; c <= 'z'; c++) {  
            collection.add(c);  
        }  
        System.out.print("Characters : ");  
        System.out.print("[");  
        Iterator<Character> iterator = collection.iterator();  
        //will return the iterator for all characters  
        while (iterator.hasNext()) {  
            System.out.print(iterator.next()+" ,");  
        }  
        System.out.println("]"); 
	}

}

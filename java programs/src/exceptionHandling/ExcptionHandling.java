package exceptionHandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExcptionHandling{

	public static void main(String[] args) {
		try {
			int a = 30, b = 0;
			int c = a / b; // cannot divide by zero
			System.out.println("Result = " + c);

			String str = null; // null value
			System.out.println(str.charAt(0));

			String str1 = "This is very cheesy"; // length is 22
			char ch = str1.charAt(24); // accessing 25th element
			System.out.println(ch);

			// Following file does not exist
			File file = new File("E://file.txt");
			FileReader fr = new FileReader(file);

			int num = Integer.parseInt("akki");

			int arr[] = new int[5];
			arr[6] = 9;

			int[] myNumbers = { 1, 2, 3 };
			System.out.println(myNumbers[10]);

		} catch (ArithmeticException e) {
			System.out.println("Can't divide a number by 0");
		} catch (NullPointerException e) {
			System.out.println("NullPointerException..");
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("StringIndexOutOfBoundsException");
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist");
		} catch (NumberFormatException e) {
			System.out.println("Number format exception");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array Index is Out Of Bounds");
		} catch (Exception e) {
			System.out.println("Something went wrong.");
		}
	}

}

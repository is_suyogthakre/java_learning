package strings;

public class string {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1 = "java";// creating string by java string literal
		char ch[] = { 's', 't', 'r', 'i', 'n', 'g', 's' };
		String s2 = new String(ch);// converting char array to string
		String s3 = new String("example");// creating java string by new keyword
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

		System.out.println(s1.length());
		System.out.println(s2.concat(s1));
		System.out.println(s1.substring(1));
		System.out.println(s1.substring(1, 2));

		System.out.println(s1.replace('a', 'e'));
		System.out.println(s1.replace("in", "within"));
		System.out.println(s1.trim());
		System.out.println(s1.toUpperCase());
		System.out.println(s1.toLowerCase());
	}

}
